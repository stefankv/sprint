import React from 'react';
import { createRoot } from 'react-dom/client';
import { Navbar, Nav, Button, Container } from 'react-bootstrap';
import { Route, Link, BrowserRouter as Router, Routes, Navigate } from 'react-router-dom';
import { logout } from './services/auth';
import Home from './components/Home'
import Tasks from './components/tasks/Tasks';




class App extends React.Component {

    render() {
        return(
            <div>
            <Router>
                <Navbar expand bg="dark" variant="dark">
                    <Navbar.Brand as={Link} to="/">
                    Welcome to JWD Tasks
                    </Navbar.Brand>
                <Nav>
                    <Nav.Link as={Link} to="/tasks">
                        Tasks
                    </Nav.Link>
                    <Button onClick={() => logout()}>Log out</Button>
                </Nav> 
                </Navbar>
                <Container style={{paddingTop:"10px"}}>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/login" element={<Navigate replace to ="/tasks"/>}/>
                        <Route path="/tasks" element={<Tasks/>}/>
                    </Routes>
                </Container>
            </Router>
        </div>
        )
    }

}


const rootElement = document.getElementById('root')
const root = createRoot(rootElement)

root.render(
    <App/>,
)