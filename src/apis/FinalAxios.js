import axioos from 'axios';

let FinalAxios = axioos.create({
    baseURL: 'http://localhost:8080/api'
})

FinalAxios.interceptors.request.use(
    function intercept(config) {
        const jwt = window.localStorage['jwt']
        if(jwt) {
            config.headers['Authorization'] = "Bearer " + jwt
        }
        return config;
    } 
);
export default FinalAxios;