import React from 'react'
import FinalAxios from '../../apis/FinalAxios'
import {Col, Row, Table, Button, Form} from 'react-bootstrap'
import {withParams, withNavigation} from '../../routeconf'

class Tasks extends React.Component {
    constructor(props) {
        super(props)

        let add = {
            name:"",
            inCharge:"",
            points: 0,
            state: null,
            sprint: null
        }

        this.state = {tasks:[], states:[], sprints:[], adds: add}
    }

    componentDidMount() {
        this.getTasks()
        this.getSprints()
        this.getStates()
    }

    getTasks() {
        FinalAxios.get('/zadaci')
            .then(res => {
                console.log(res.data);
                this.setState({tasks: res.data})
            })
            .catch(error => console.log(error))
    }

    getStates() {
        FinalAxios.get('/stanja')
            .then(res => {
                console.log(res.data);
                this.setState({states: res.data})
            })
            .catch(error => console.log(error))
    }

    getSprints() {
        FinalAxios.get('/sprintovi')
            .then(res => {
                console.log(res.data);
                this.setState({sprints: res.data})
            })
            .catch(error => console.log(error))
    }

    onInputChange(e) {
        const name = e.target.name;
        const value = e.target.value
      
        let adds = this.state.adds
        adds[name] = value;
      
        this.setState({adds})
      }

    create = () => {
        let dto = {
            ime: this.state.adds.name,
            zaduzeni: this.state.adds.inCharge,
            bodovi: this.state.adds.points,
            stanjeId: this.state.adds.state,
            sprintId: this.state.adds.sprint
        }

        FinalAxios.post("/zadaci", dto) 
            .then(res => {
                console.log(res);
                alert("Assignemnt was successfully added!")
                this.getTasks()
            })
            .catch(error => {
                alert("Error occured please try again!")
                console.log(error);
            })
    }


    renderTasks() {
        return this.state.tasks.map((task, index) => {
            return(
                <>
                <tr key={task.id}>
                    <td>{task.ime}</td>
                    <td>{task.zaduzeni}</td>
                    <td>{task.bodovi}</td>
                    <td>{task.stanjeIme}</td>
                    <td>{task.sprintIme}</td>
                    <td><Button>Change state</Button></td>
                    <td><Button variant="warning">Edit</Button></td>
                    <td><Button variant="danger">Delete</Button></td>
                </tr>
                </>
            )
        })
    }

    renderCreateForm(){
        return (
          <>
          <Row>
            <Col></Col>
            <Col xs="3">
            <h1>Add new Assignemnt</h1>
            <Form>
              <Form.Group>
              <Form.Label htmlFor="name">Name</Form.Label>
              <Form.Control id="name" name="name" onChange={(e) => this.onInputChange(e)}/> <br/>
              </Form.Group>
              <Form.Group>
              <Form.Label htmlFor="inCharge">In charge</Form.Label>
              <Form.Control id="inCharge" name="inCharge" onChange={(e) => this.onInputChange(e)}/> <br/>
              </Form.Group>
              <Form.Group>
              <Form.Label htmlFor="points">Points</Form.Label>
              <Form.Control id="points" type="number" name="points" onChange={(e) => this.onInputChange(e)}/> <br/>
              </Form.Group>
              <Form.Group>
              <Form.Label htmlFor="state">State</Form.Label>
              <Form.Control as="select" id="state" name="state" onChange={(e) => this.onInputChange(e)}>
                <option></option>
                {
                    this.state.states.map((state, index) => {
                      return (
                        <option key={state.id} value={state.id}>{state.ime}</option>
                      )
                    })
                }
                </Form.Control> <br/>
              </Form.Group>
              <Form.Group>
              <Form.Label htmlFor="sprint">Sprint</Form.Label>
              <Form.Control as="select" id="sprint" name="sprint" onChange={(e) => this.onInputChange(e)}>
                <option></option>
                {
                    this.state.sprints.map((sprint, index) => {
                      return (
                        <option key={sprint.id} value={sprint.id}>{sprint.ime}</option>
                      )
                    })
                }
                </Form.Control> <br/>
              </Form.Group>
              <Button type="reset" style={{ marginTop: "25px" }} onClick={this.create}>Add</Button>
            </Form>        
            </Col>
            <Col></Col>
          </Row>
          </>
        )
      } 

    render() {
        return(
            <>
                <Col>
                    <Row><h1>Task App</h1></Row>
                    {this.renderCreateForm()}
                    <br/>
                    <br/>
                    <Row>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>In charge</th>
                                    <th>Points</th>
                                    <th>State</th>
                                    <th>Sprint</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderTasks()}
                            </tbody>
                        </Table>
                    </Row>
                </Col>
            </>
        )
    }
}

export default withNavigation(withParams(Tasks))